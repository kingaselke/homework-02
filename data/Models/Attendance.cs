﻿using System.ComponentModel.DataAnnotations;
using DataLayer.Models;

namespace dane.Models
{
    public class Attendance
    {
        public int Id { get; set; }
        public enum PersonAttendance { Obecny, Nieobecny }
        public double PresenceNumber { get; set; }
        public double Days { get; set; }
        public PersonAttendance PersonAttendances { get; set; }
        public Course Course { get; set; }
        public Person Person { get; set; }
    }
}
