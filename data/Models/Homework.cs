﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Models
{
    public class Homework
    {
        public int Id { get; set; }
        public double MaxNumberOfPoints { get; set; }
        public double NumberOfPoints { get; set; }
        public Course Course { get; set; }
        public Person Person { get; set; }
    }
}
