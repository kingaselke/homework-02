﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using dane.Models;

namespace DataLayer.Models
{
    public class Person
    {
        public int Id { get; set; }
        public enum PersonSex { M, F }
        public long Pesel { get; set; }
        public string StudentName { get; set; }
        public string StudentSurname { get; set; }
        public DateTime Birthday { get; set; }
        public PersonSex Sex { get; set; }
        public List<Course> ListAllCourses { get; set; }
        public List<Homework> ListHomework { get; set; }
        public List<Attendance> ListAttendance { get; set; }
    }
}
