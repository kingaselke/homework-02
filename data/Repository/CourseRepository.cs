﻿using System.Collections.Generic;
using DataLayer.DbContext;
using DataLayer.Models;
using System.Linq;
using System.Data.Entity;
using System.Security.Policy;
using dane.Models;

namespace dane.Repository
{
    public class CourseRepository
    {
        public static bool AddCourse(Course course)
        {
            var rowsAffected = 0;

            using (var dbContext = new CourseDbContext())
            {
                List<Person> listOfPeople = new List<Person>();
                foreach (var student in course.AllStudentList)
                {
                    Person peopleBase = new Person();
                    peopleBase = dbContext.PersonsDbSet.First(s => s.Pesel == student.Pesel);
                    listOfPeople.Add(peopleBase);
                }
                course.AllStudentList = listOfPeople;
                dbContext.CoursesDbSet.Add(course);
                rowsAffected = dbContext.SaveChanges();
            }

            return rowsAffected == 1;
        }

        public static bool AddStudent(Person person)
        {
            var rowsAffected = 0;

            using (var dbContext = new CourseDbContext())
            {
                dbContext.PersonsDbSet.Add(person);
                rowsAffected = dbContext.SaveChanges();
            }

            return rowsAffected == 1;
        }

        public static bool AddHomework(Homework homework)
        {
            var rowsAffected = 0;

            using (var dbContext = new CourseDbContext())
            {
                
                dbContext.HomeworksDbSet.Add(homework);
                rowsAffected = dbContext.SaveChanges();
            }

            return rowsAffected == 1;
        }

        public static bool AddAttendance(Attendance attendance)
        {
            var rowsAffected = 0;

            using (var dbContext = new CourseDbContext())
            {

                dbContext.AttendancesDbSet.Add(attendance);
                rowsAffected = dbContext.SaveChanges();
            }

            return rowsAffected == 1;
        }

        public static bool AddStudentToCourse(Course course)
        {
            var rowsAffected = 0;

            using (var dbContext = new CourseDbContext())
            {
                var query = dbContext.CoursesDbSet.Include(y => y.AllStudentList);
                var courses = query.First(x => x.Id == course.Id);
                foreach (Person s in course.AllStudentList)
                {
                    var students = dbContext.PersonsDbSet.First(x => x.Id == s.Id);
                    //courses.AllStudentList.Add(students);
                }
                rowsAffected = dbContext.SaveChanges();
            }

            return rowsAffected == 1;
        }

        public static bool IsPeselInDb(long pesel)
        {
            using (var dbContext = new CourseDbContext())
            {
                var peselDb = dbContext.PersonsDbSet.Count(x => x.Pesel == pesel);
                if (peselDb == 0)
                    return false;
                return true;
            }
        }
        public static bool IsIdlInDb(int id)
        {
            using (var dbContext = new CourseDbContext())
            {
                var idDb = dbContext.PersonsDbSet.Count(x => x.Id == id);
                if (idDb == 0)
                    return false;
                return true;
            }
        }

    

        public static List<Person> GetAllStudents()
        {
            var person = new List<Person>();
            using (var dbContext = new CourseDbContext())
            {
                person = dbContext.PersonsDbSet.ToList(); // pobierz all z bazy danych

            }
            return person;
        }
        public static List<Course> GetAllCourses()
        {
            var course = new List<Course>();
            using (var dbContext = new CourseDbContext())
            {
                course = dbContext.CoursesDbSet.ToList(); // pobierz all z bazy danych
            }
            return course;
        }

        

        public Course GetAllCourseDataById(int id)
        {
            Course course = null;

            using (var dbContext = new CourseDbContext())
            {
                course = dbContext.CoursesDbSet
                    .Include(bs => bs.AllStudentList.Select(b => b.StudentName))
                    .Single(bs => bs.Id == id);
            }

            return course;
        }

    }


   
}
