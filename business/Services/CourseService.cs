﻿using business.Dtos;
using business.Mappers;
using dane.Repository;
using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using dane.Models;


namespace business.Services
{
    public class CourseService
    {
        public static void HomeworkCliToMappers(HomeworkDTo homeworkDTo)
        {

            Homework homework = Mapper.HomeworkDtoToHomework(homeworkDTo);
            CourseRepository.AddHomework(homework);
        }
        public static void AttendanceCliToMappers(AttendanceDTo attendanceDTo)
        {

            Attendance attendance = Mapper.AttendanceDtoToAttendance(attendanceDTo);
            CourseRepository.AddAttendance(attendance);
        }
        public static void CourseCliToMappers(CourseDTo courseDTo)
        {

            Course course = Mapper.CourseDToToCourse(courseDTo);
            CourseRepository.AddCourse(course);
            // CourseRepository.AddStudentToCourse(course);
        }

        public static void PersonCliToMappers(PersonDto personDTo)
        {
            if (CourseRepository.IsPeselInDb(personDTo.Pesel))

                return;

            Person person = Mapper.PersonDToToPerson(personDTo);
            CourseRepository.AddStudent(person);
        }

        public CourseDTo GetCourseData(int id)
        {
            var courseRepository = new CourseRepository();
            var course = courseRepository.GetAllCourseDataById(id);
            return Mapper.CourseToCourseDto(course);
        }


        public static bool IsPeselInDb(long pesel)
        {
            return CourseRepository.IsPeselInDb(pesel);

        }
        public static bool IsIdInDb(int id)
        {
            return CourseRepository.IsIdlInDb(id);

        }

        public static List<PersonDto> GetAllStudents()
        {
            return Mapper.ListPersonToListPersonDto(CourseRepository.GetAllStudents());
        }

        public static List<CourseDTo> GetAllCourses()
        {
            return Mapper.ListCourseToListCourseDto(CourseRepository.GetAllCourses());
        }


        //public static List<PersonDto> GetAllStudentsFromCourse(CourseDTo course)
        //{
        //    return Mapper.ListPersonToListPersonDto( 
        //        CourseRepository.GetAllStudentsFromCourse(
        //            Mapper.CourseDToToCourse(course)
        //            ));
        //}
    }
}
