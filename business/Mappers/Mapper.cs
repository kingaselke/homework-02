﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using business.Dtos;
using dane.Models;
using DataLayer.Models;

namespace business.Mappers
{
    public class Mapper
    {


        public static CourseDTo CourseToCourseDto(Course course)
        {
            var courseToDto = new CourseDTo();
            courseToDto.Id = course.Id;
            courseToDto.CourseName = course.CourseName;
            courseToDto.CourseLeader = course.CourseLeader;
            courseToDto.CourseStartDate = course.CourseStartDate;
            courseToDto.ThresholdPoint = course.ThresholdPoint;
            courseToDto.Presence = course.Presence;
            courseToDto.NumberOfStudents = course.NumberOfStudents;
            courseToDto.ListOfStudents = ListPersonToListPersonDto(course.AllStudentList);
           return courseToDto;
        }
        public static Course CourseDToToCourse(CourseDTo courseDTo)
        {
            var course = new Course();
            course.Id = courseDTo.Id;
            course.CourseName = courseDTo.CourseName;
            course.CourseLeader = courseDTo.CourseLeader;
            course.CourseStartDate = courseDTo.CourseStartDate;
            course.ThresholdPoint = courseDTo.ThresholdPoint;
            course.Presence = courseDTo.Presence;
            course.NumberOfStudents = courseDTo.NumberOfStudents;
            course.AllStudentList = ListPersonDtoToListPerson(courseDTo.ListOfStudents);
            return course;
        }

        public static PersonDto PersonToPersonDto(Person person)
        {
            var personToDto = new PersonDto();
            personToDto.Id = person.Id;
            personToDto.StudentName = person.StudentName;
            personToDto.StudentSurname = person.StudentSurname;
            personToDto.Pesel = person.Pesel;
            personToDto.Birthday = person.Birthday;
            personToDto.Sex = (PersonDto.PersonSex)person.Sex;

            return personToDto;
        }
        public static Person PersonDToToPerson(PersonDto personDto)
        {
            var person = new Person();
            person.Id = person.Id;
            person.StudentName = personDto.StudentName;
            person.StudentSurname = personDto.StudentSurname;
            person.Pesel = personDto.Pesel;
            person.Birthday = personDto.Birthday;
            person.Sex = (Person.PersonSex)personDto.Sex;
            person.ListAllCourses = ListCourseDtoToListCourse(PersonDto.ListOfCourses);
            return person;
        }


        public static List<CourseDTo> ListCourseToListCourseDto(List<Course> listCourse)
        {
            List<CourseDTo> list = new List<CourseDTo>();
            if (listCourse == null)
            {
                return list;
            }
            foreach (var student in listCourse)
            {
                list.Add(CourseToCourseDto(student));
            }
            return list;
        }
        public static List<Course> ListCourseDtoToListCourse(List<CourseDTo> listCourseDto)
        {
            List<Course> list = new List<Course>();
            if (listCourseDto == null)
            {
                return list;
            }
            foreach (var student in listCourseDto)
            {
                list.Add(CourseDToToCourse(student));
            }
            return list;
        }

        public static List<PersonDto> ListPersonToListPersonDto(List<Person> listPerson)
        {
            List<PersonDto> list = new List<PersonDto>();
            if (listPerson == null)
            {
                return list;
            }

            foreach (var student in listPerson)
            {
                list.Add(PersonToPersonDto(student));
            }
            return list;
        }
        public static List<Person> ListPersonDtoToListPerson(List<PersonDto> listPersonDto)
        {
            List<Person> list = new List<Person>();
            if (listPersonDto == null)
            {
                return list;
            }
            foreach (var student in listPersonDto)
            {
                list.Add(PersonDToToPerson(student));
            }
            return list;


        }

        public static List<AttendanceDTo> ListAttendanceToListAttendanceDto(List<Attendance> listAttendances)
        {
            List<AttendanceDTo> list = new List<AttendanceDTo>();
            if (listAttendances == null)
            {
                return list;
            }

            foreach (var student in listAttendances)
            {
                list.Add(AttendanceToattendanceDTo(student));
            }
            return list;
        }
        public static List<Attendance> ListAttendanceDtoToListAttendance(List<AttendanceDTo> listAttendanceDTo)
        {
            List<Attendance> list = new List<Attendance>();
            if (listAttendanceDTo == null)
            {
                return list;
            }

            foreach (var student in listAttendanceDTo)
            {
                list.Add(AttendanceDtoToAttendance(student));
            }
            return list;
        }


        public static List<HomeworkDTo> ListHomeworkToListHomeworkDto(List<Homework> listHomeworks)
        {
            List<HomeworkDTo> list = new List<HomeworkDTo>();
            if (listHomeworks == null)
            {
                return list;
            }

            foreach (var student in listHomeworks)
            {
                list.Add(HomeworkTohomeworkDto(student));
            }
            return list;
        }
        public static List<Homework> ListHomeworkDToToListHomework(List<HomeworkDTo> listHomeworkDTo)
        {
            List<Homework> list = new List<Homework>();
            if (listHomeworkDTo == null)
            {
                return list;
            }

            foreach (var student in listHomeworkDTo)
            {
                list.Add(HomeworkDtoToHomework(student));
            }
            return list;
        }

        public static Homework HomeworkDtoToHomework(HomeworkDTo homeworkDTo)
        {
            var homework = new Homework();
            homework.Id = homeworkDTo.Id;
            homework.MaxNumberOfPoints = homeworkDTo.MaxNumberOfPoints;
            homework.NumberOfPoints = homeworkDTo.NumberOfPoints;
            homework.Course = CourseDToToCourse(homeworkDTo.Course);
            homework.Person = PersonDToToPerson(homeworkDTo.Person);
            return homework;
        }
        public static HomeworkDTo HomeworkTohomeworkDto(Homework homework)
        {
            var homeworkToDto = new HomeworkDTo();
            homeworkToDto.Id = homework.Id;
            homeworkToDto.MaxNumberOfPoints = homework.MaxNumberOfPoints;
            homeworkToDto.NumberOfPoints = homework.NumberOfPoints;
            homeworkToDto.Course = CourseToCourseDto(homework.Course);
            homeworkToDto.Person = PersonToPersonDto(homework.Person);
            return homeworkToDto;
        }

        public static AttendanceDTo AttendanceToattendanceDTo(Attendance attendance)
        {
            var attendanceDTo = new AttendanceDTo();
            attendanceDTo.Id = attendance.Id;
            attendanceDTo.PersonAttendances = (AttendanceDTo.PersonAttendance)attendance.PersonAttendances;
            attendanceDTo.PresenceNumber = attendance.PresenceNumber;
            attendanceDTo.Days = attendance.Days;
            attendanceDTo.Course = CourseToCourseDto(attendance.Course);
            attendanceDTo.Person = PersonToPersonDto(attendance.Person);
            return attendanceDTo;
        }
        public static Attendance AttendanceDtoToAttendance(AttendanceDTo attendanceDTo)
        {
            var attendance = new Attendance();
            attendance.Id = attendanceDTo.Id;
            attendance.PersonAttendances = (Attendance.PersonAttendance)attendanceDTo.PersonAttendances;
            attendance.PresenceNumber = attendanceDTo.PresenceNumber;
            attendance.Days = attendanceDTo.Days;
            attendance.Course = CourseDToToCourse(attendanceDTo.Course);
            attendance.Person = PersonDToToPerson(attendanceDTo.Person);
            return attendance;
        }
    }
}
