﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace business.Dtos
{
  public  class AttendanceDTo
    {
        public int Id;
        public enum PersonAttendance { Obecny, Nieobecny }
        public double PresenceNumber;
        public double Days;
        public PersonAttendance PersonAttendances;
        public PersonDto Person;
        public CourseDTo Course;
    }
}
