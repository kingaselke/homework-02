﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace business.Dtos
{
    public class CourseDTo
    {
        public int Id;
        public string CourseName;
        public string CourseLeader;
        public DateTime CourseStartDate;
        public int ThresholdPoint;
        public int Presence;
        public int NumberOfStudents;
        public List <PersonDto> ListOfStudents = new List<PersonDto>();
        public List<HomeworkDTo> ListHomework = new List<HomeworkDTo>();
        public List<AttendanceDTo> LisAttendance = new List<AttendanceDTo>();

    }
}
