﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadanie2.Helpers
{
    public class ConsoleReadHelper
    {
        public static long GetLong(string message)
        {
            while (true)
            {
                Console.WriteLine(message);
                long value;
                if (long.TryParse(Console.ReadLine(), out value))
                    return value;
            }
        }
        public static int GetInt(string message)
        {
            while (true)
            {
                Console.WriteLine(message);
                int value;
                if (int.TryParse(Console.ReadLine(), out value))
                    return value;
            }
        }
        public static DateTime GetDate(string message)
        {
            while (true)
            {
                Console.WriteLine(message);
                DateTime value;
                if (DateTime.TryParse(Console.ReadLine(), out value))
                    return value;
            }
        }
        public static ProgramLoop.CommandTypes GetCommnadType(string message)
        {
            ProgramLoop.CommandTypes commandType;
            Console.Write(message + " (AddPerson, AddCourse, AddHomework, AddAtendance, Exit) ");

            while (!Enum.TryParse(Console.ReadLine(), out commandType))
            {
                Console.WriteLine("Not a proper command type - try again");
            }

            return commandType;
        }
    }
}
