﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using business.Dtos;
using business.Mappers;
using business.Services;
using Zadanie2.Helpers;

namespace Zadanie2
{
    public class ProgramLoop
    {

        public enum CommandTypes { AddPerson, AddCourse, AddHomework, AddAttendance, PrintRaport, Exit }

        public void Execute()
        {
            var exit = false;
            while (!exit)
            {
                var command = Helpers.ConsoleReadHelper.GetCommnadType("Provide a command: ");

                switch (command)
                {
                    case CommandTypes.AddPerson:
                        AddPerson();
                        break;
                    case CommandTypes.AddCourse:
                        AddCourse();
                        break;
                    case CommandTypes.AddHomework:
                        AddHomework();
                        break;
                    case CommandTypes.AddAttendance:
                        AddAttendance();
                        break;
                    case CommandTypes.PrintRaport:
                        PrintRaport();
                        break;
                    case CommandTypes.Exit:
                        exit = true;
                        break;
                    default:
                        Console.WriteLine("Command " + command + " is not supported");
                        break;
                }
            }
        }

        private void PrintRaport()
        {
            var courseId = ConsoleReadHelper.GetInt("Podaj Id kursu");
            var courseService = new CourseService();
            var course = courseService.GetCourseData(courseId);
            ConsoleWriteHelper.PrintCourseDto(course);



        }


        private void AddPerson()
        {
            var personDto = new PersonDto();
            Console.WriteLine("podaj imię");
            personDto.StudentName = Console.ReadLine();
            Console.WriteLine("podaj nazwisko");
            personDto.StudentSurname = Console.ReadLine();
            personDto.Pesel = Helpers.ConsoleReadHelper.GetLong("podaj pesel");
            personDto.Birthday = Helpers.ConsoleReadHelper.GetDate("podaj datę ur");
            bool work = true;
            while (work)
            {
                Console.WriteLine("podaj płeć: F/M ");
                var sex = Console.ReadLine();
                if (sex == "M" || sex == "F")
                {
                    personDto.Sex = (PersonDto.PersonSex)Enum.Parse(typeof(PersonDto.PersonSex), sex);
                    work = false;
                }
                else
                    Console.WriteLine("Błędne dane");
            }
            CourseService.PersonCliToMappers(personDto);
        }
        private void AddAttendance()
        {
            List<CourseDTo> allCourse = CourseService.GetAllCourses();

            var attendanceDto = new AttendanceDTo();

            while (true)
            {
                Console.WriteLine();
                int courseId = Helpers.ConsoleReadHelper.GetInt("podaj id kursu ");
                var selected = allCourse.Where(x => x.Id == courseId);

                if (!selected.Any())
                {
                    continue;
                }
                attendanceDto.Course = selected.First();

                break;
            }

            List<PersonDto> persons = CourseService.GetAllStudents();
            while (true)
            {
                Console.WriteLine("podaj id studenta");
                int StudentId = Helpers.ConsoleReadHelper.GetInt("podaj id studenta ");
                var selected = persons.Where(x => x.Id == StudentId);

                if (!selected.Any())
                {
                    continue;
                }
                attendanceDto.Person = selected.First();

                break;
            }


            while (true)
            {
                Console.WriteLine("podaj czy student był obecny:");
                var attendance = Console.ReadLine();
                if (attendance == "obecny")
                {
                    attendanceDto.PersonAttendances =
                        (AttendanceDTo.PersonAttendance)Enum.Parse(typeof(AttendanceDTo.PersonAttendance), attendance);

                    attendanceDto.PresenceNumber=1;
                    attendanceDto.Days = 1;

                }
                else if (attendance == "nieobecny")
                {
                    attendanceDto.Days++;
                }
                break;
            }
            CourseService.AttendanceCliToMappers(attendanceDto);
        }
        
        private void AddHomework()
        {
            List<CourseDTo> allCourse = CourseService.GetAllCourses();

            var homeworkDto = new HomeworkDTo();

            while (true)
            {
                Console.WriteLine();
                int courseId = Helpers.ConsoleReadHelper.GetInt("podaj id kursu ");
                var selected = allCourse.Where(x => x.Id == courseId);

                if (!selected.Any())
                {
                    continue;
                }
                homeworkDto.Course = selected.First();

                break;
            }

            List<PersonDto> persons = CourseService.GetAllStudents();
            while (true)
            {
                Console.WriteLine("podaj id studenta");
                int StudentId = Helpers.ConsoleReadHelper.GetInt("podaj id studenta ");
                var selected = persons.Where(x => x.Id == StudentId);

                if (!selected.Any())
                {
                    continue;
                }
                homeworkDto.Person = selected.First();

                break;
            }

            homeworkDto.MaxNumberOfPoints = Helpers.ConsoleReadHelper.GetInt("podaj maxymalną liczbę punktów studenta za zadanie :");
            homeworkDto.NumberOfPoints = Helpers.ConsoleReadHelper.GetInt("podaj liczbę punktów studenta za zadanie :");
            CourseService.HomeworkCliToMappers(homeworkDto);
        }

        private void AddCourse()
        {
            var courseDto = new CourseDTo();
            Console.WriteLine("podaj nazwę kursu");
            courseDto.CourseName = Console.ReadLine();
            Console.WriteLine("podaj dane prowadzącego");
            courseDto.CourseLeader = Console.ReadLine();
            courseDto.CourseStartDate = Helpers.ConsoleReadHelper.GetDate("podaj datę rozpoczęcia kursu");
            courseDto.ThresholdPoint = Helpers.ConsoleReadHelper.GetInt("podaj próg dla prac domowych");
            courseDto.Presence = Helpers.ConsoleReadHelper.GetInt("podaj próg dla obecności");
            courseDto.NumberOfStudents = Helpers.ConsoleReadHelper.GetInt("podaj liczbę studentów");

            List<PersonDto> AllStudentsList = CourseService.GetAllStudents();

            int count = 0;
            while (count < courseDto.NumberOfStudents)
            {
                long pesel = Helpers.ConsoleReadHelper.GetLong("podaj pesel studenta");

                var selectedStudent = AllStudentsList.Where(x => x.Pesel == pesel);
                if (!selectedStudent.Any())

                    continue;


                if (courseDto.ListOfStudents.Contains(selectedStudent.First()))

                    continue;

                courseDto.ListOfStudents.Add(selectedStudent.First());
                count++;

            }
            // Console.WriteLine(courseDto.ListOfStudents);    
            CourseService.CourseCliToMappers(courseDto);

        }
    }
}
